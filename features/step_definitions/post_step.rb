# Before do
#     @user = User.find(1)
# end    
    
    Given ('I am on create post page') do
        @params = {
            title: "title",
            content: "content"
        }
    end

    When('I open detail post in home page') do
        Post.first
    end

    When('I access home page') do
        visit root_path
    end
    
    Then('I should see post list') do
        Post.all
    end

    Given('I have post') do 
        Post.create(title: title)
        
    end
    
    When('I fill in {string} with {string}') do |attribute, value|
        @params[attribute] = value
 
    end

    When('I created post') do
        @post = Post.new(@params)
        @post.user = @user
        @response = @post.save
    end

    When('I press delete post in post page with {string} = {string}') do |post_id, id|
        @post = Post.find_by_id(1)
        puts @post
    end
    
    Then('I should not see my post in home page') do
        @post.destroy
        # expect(@response).to eq(false)
    end

    Then('I got an error message') do
        @post = Post.new(@params)
    end

    Then('I should see post details') do
        @post = Post.find_by_id(1)
        
    end