Feature: Make Post
    In order to make a post
    As an author
    I want to create and manage post

    # Background:
    #     Given I am logged in as "ada1@mail.com" with password "12345678"

    Scenario: post list
        When I access home page
        Then I should see post list

    Scenario: create post
        Given I am on create post page
        When I fill in "title" with "totally"
        And I fill in "content" with "apapun itu"
        Then I created post

    Scenario: can not create post with blank title
        Given I am on create post page
        When I fill in "title" with ""
        And I fill in "content" with "apapun itu"
        Then I created post
        And I got an error message

    Scenario: can not create post with blank title
        Given I am on create post page
        When I fill in "title" with "title"
        And I fill in "content" with ""
        Then I created post
        And I got an error message

    Scenario: show post
        Given I have post
        When I open detail post in home page
        Then I should see post details

    Scenario: delete post
        Given I have post
        When I open detail post in home page
        And I press delete post in post page with "id" = "1"
        Then I should not see my post in home page


