require 'rails_helper'

RSpec.describe User, type: :model do
  context 'validation tests' do
    it 'ensures emails presence' do
      user = User.new(email:'ada1@mail.com', password:'12345678')
      expect(user.valid?).to eq(true)
    end
    it 'ensures password presence' do
      user = User.new(email:'ada1@mail.com', password:'123123123')
      expect(user.valid?).to eq(true)
    end
    it 'ensures emails does not presence' do
      user = User.new(email:'', password:'12345678')
      expect(user.valid?).to eq(false)
    end
    it 'ensures password does not presence' do
      user = User.new(email:'ada1@mail.com', password:'')
      expect(user.valid?).to eq(false)
    end


  end
end
