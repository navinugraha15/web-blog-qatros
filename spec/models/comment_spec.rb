require 'rails_helper'

RSpec.describe Comment, type: :model do
  subject { Comment.new(content:'apaaja', user_id: 1, post_id:1)}

  before{subject.save}

  it 'content should be presence with minimum length 5 char' do
    subject.content = 'lima huruf'
    expect(subject).to be_valid
  end

  it 'content not presence with minimum length 5 char' do
    subject.content = ''
    expect(subject).to_not be_valid
  end
end
