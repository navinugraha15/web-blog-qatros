require 'rails_helper'

RSpec.describe Post, type: :model do
  # before(:each) do
  # @user = User.create(
  #   email: "abc@mail.com",
  #   password: "123"
  # )
  # end
  subject { Post.new(title:'title', content:'apaaja', user_id: 1)}

  before{subject.save}

  it 'title and content should be presence' do
    subject.title = 'title'
    subject.content = 'lima huruf'
    expect(subject).to be_valid
  end

  it 'title should be presence' do
    subject.title = 'title'
    expect(subject).to be_valid
  end

  it 'title not presence' do
    subject.title = ''
    expect(subject).to_not be_valid
  end
  
  it 'content should be presence with minimum length 5 char' do
    subject.content = 'lima huruf'
    expect(subject).to be_valid
  end

  it 'content not presence with minimum length 5 char' do
    subject.content = ''
    expect(subject).to_not be_valid
  end



end
