class User < ApplicationRecord
  has_many :posts
  has_many :comments
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable,
         :registerable, :confirmable
  
  
  validates :email, presence: true
  validates :password, presence: true
  validates_associated :posts, :comments
  

  


end
