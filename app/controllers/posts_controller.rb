class PostsController < ApplicationController
    before_action :find_post, only: %i[ show edit update destroy ]
    
    def index
        @posts = Post.all.order('created_at DESC')
    end

    def new
        @post = Post.new
    end

    def show
    end

    def create
        @post = Post.new(post_params.merge(user_id: current_user.id))
    
          if @post.save
            redirect_to posts_url, notice: "Post was successfully created."
          else
            render :new, status: :unprocessable_entity 
          end

    end

    def update
      if @post.update(post_params) 
      redirect_to post_url(@post), notice: "Post has been updated"
      else
        render :edit, status: :unprocessable_entity
      end
    end

    def edit
        
    end

    def destroy
        @post.destroy
        redirect_to posts_url(@post), "Post has been deleted"
    end

    private
    # Use callbacks to share common setup or constraints between actions.
    def find_post
      @post = Post.find(params[:id])
      rescue ActiveRecord::RecordNotFound
        redirect_to posts_path, notice: "Post no found"
    end

    # Only allow a list of trusted parameters through.
    def post_params
      params.require(:post).permit(:title, :content)
    end

    
end