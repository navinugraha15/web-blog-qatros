class CommentsController < ApplicationController
    # before_action :set_comment, only: %i[ show edit update destroy ]

    def index
    end

    def create
        @post = Post.find(params[:post_id])
        @comment = @post.comments.new(comment_params.merge(user_id: current_user.id, post_id: params[:post_id]))
        
        if @comment.save
            redirect_to post_url(@post), notice: "Comment has been added"
        else
            redirect_to post_url(@post)
        end
    end

    def destroy
        @post = Post.find(params[:post_id])
        @comment = @post.comments.find(params[:id])
        @comment.destroy
        redirect_to posts_path(@post)
    end

    private

    def set_comment
        @comment = Comment.find(params[:id])
        rescue ActiveRecord::RecordNotFound
          redirect_to posts_path, notice: "NO comments"
      end

    def comment_params
        params.require(:comment).permit(:content)
      end

end
